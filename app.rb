require 'sinatra'
require 'erubis'
require 'active_record'

set :sessions, true
set :root, File.dirname(__FILE__)
set :public_folder, Proc.new { File.join(root, "static") }
set :views, Proc.new { File.join(root, "views") }
set :show_exceptions, false
set :raise_errors, false

ActiveRecord::Base.establish_connection(
	:adapter => 'sqlite3',
	:database =>  'db/page.sqlite3.db'
)

class Posts < ActiveRecord::Base
end

class Pics < ActiveRecord::Base
end

get '/' do
	@posts = Posts.last(3).reverse
	@title = "Shiroeni | Главная"
	erb :index
end

get '/me' do
	@title = "Shiroeni | Обо мне"
	erb :me
end

get '/blog' do
	@posts = Posts.last(10).reverse
	@t = "Shiroeni | Блог"
	erb :blog
end

#TODO: Сделать пагинацию
#FIXME: Починить получение постов
get '/blog/:num' do
	if params[:num] == 1 then
		@posts = Posts.last(10).reverse
	else
		ids = params[:num]*10
		@posts = Posts.find(ids, ids + 9)
	end
	@t = "Shiroeni | Блог"
	erb :blog
end

get '/post/:num' do
	@posts = Posts.find(params[:num])
	@title = "Shiroeni | " + params[:num]
	erb :post
end

get '/addpost' do
	erb :addpost
end

post '/addpost' do
	#FIXME: Не очень хороший способ автоинкремента, исправить
	#FIXME: Изменить систему защиты паролем
	passwd = 'passwd'
	if params=[:passwd] == passwd then
		id = Posts.last
		new_id = 1 + id.id
		head = params[:head]
		body = params[:body]
		new_record = Posts.create(id: new_id, head: params[:head], body: params[:body], posttime: Time.now)
		"Сообщение успешно опубликовано!"
	else
		"Пароль не правильный"
	end
end

get '/contacts' do
	@title = "Shiroeni | Контакты"
	erb :contacts
end

get '/projects' do
	@title = "Shiroeni | Мои проекты"
	erb :projects
end

get '/404' do
	erb :not_found
end

error 404 do
	redirect to('/404')
end